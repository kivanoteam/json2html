from collections import OrderedDict
import json

import pytest

from json2html import render_json


samples = [
    (
        """
        [
            {
                "h3": "Title #1",
                "div": "Hello, World 1!"
            }
        ]
        """,
        '<ul><li><h3>Title #1</h3><div>Hello, World 1!</div></li></ul>',
    ),
    (
        """
        [
            {
                "h3": "Title #1",
                "div": "Hello, World 1!"
            },
            {
                "h3": "Title #2",
                "div": "Hello, World 2!"
            }
        ]
        """,
        '<ul>' +
        '<li><h3>Title #1</h3><div>Hello, World 1!</div></li>' +
        '<li><h3>Title #2</h3><div>Hello, World 2!</div></li>' +
        '</ul>',
    ),
    (
        """
        [
            {
                "span": "Title #1",
                "content": [
                    {

                        "p": "Example 1",
                        "header": "header 1"
                    }
                ]
            }, {"div": "div 1"}
        ]
        """,
        '<ul>' +
        '<li><span>Title #1</span>' +
        '<content><ul><li><p>Example 1</p><header>header 1</header></li></ul></content>' +  # noqa E501
        '</li>' +
        '<li><div>div 1</div></li>' +
        '</ul>',
    ),
    (
        """
        {
            "p": "hello1"
        }
        """,
        '<p>hello1</p>',
    ),
    (
        """
        {
            "p.my-class#my-id": "hello",
            "p.my-class1.my-class2": "example<a>asd</a>"
        }
        """,
        '<p id="my-id" class="my-class">hello</p>' +
        '<p class="my-class1 my-class2">example&lt;a&gt;asd&lt;/a&gt;</p>',
    ),
]


@pytest.mark.parametrize('input_json,output', samples)
def test_samples(input_json, output):
    json_data = json.loads(input_json, object_pairs_hook=OrderedDict)
    assert render_json(json_data) == output
