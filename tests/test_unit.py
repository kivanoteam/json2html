import json2html as j2h


class TestParseEmmetRule:  # noqa WPS306,WPS214

    def test_empty_tag(self):
        assert j2h.parse_emmet_rule('') == ('p', '', [])

    def test_no_attributes(self):
        assert j2h.parse_emmet_rule('h1') == ('h1', '', [])

    def test_id(self):
        assert j2h.parse_emmet_rule('p#myid') == ('p', 'myid', [])

    def test_double_ids(self):
        assert j2h.parse_emmet_rule('p#myid#yourid') == ('p', 'myid', [])

    def test_class(self):
        assert j2h.parse_emmet_rule('p.myclass') == ('p', '', ['myclass'])

    def test_multiple_classes(self):
        expected = ('p', '', ['myclass', 'yourclass'])
        assert j2h.parse_emmet_rule('p.myclass.yourclass') == expected

    def test_id_and_class(self):
        expected = ('p', 'myid', ['myclass'])
        assert j2h.parse_emmet_rule('p#myid.myclass') == expected

    def test_class_and_id(self):
        expected = ('p', 'myid', ['myclass'])
        assert j2h.parse_emmet_rule('p.myclass#myid') == expected

    def test_empty_tag_with_attrs(self):
        expected = ('p', 'myid', ['myclass'])
        assert j2h.parse_emmet_rule('.myclass#myid') == expected


class TestRenderProperty:  # noqa WPS306

    def test_simple_tag(self):
        assert j2h.render_property('p', 'text') == '<p>text</p>'

    def test_id(self):
        assert j2h.render_property('p#myid', 'text') == '<p id="myid">text</p>'

    def test_id_and_class(self):
        expected = '<p id="myid" class="myclass">text</p>'
        assert j2h.render_property('p#myid.myclass', 'text') == expected

    def test_multiple_classes(self):
        expected = '<p class="myclass yourclass">text</p>'
        assert j2h.render_property('p.myclass.yourclass', 'text') == expected

    def test_render_json_call(self, monkeypatch):
        def mock_render_json(*args):
            return 'mocked text'
        monkeypatch.setattr(j2h, 'render_json', mock_render_json)
        assert j2h.render_property('p', 'text') == '<p>mocked text</p>'
