#!/usr/bin/env python

from collections import OrderedDict
import html
import json
import sys
import re

TAG_RE = re.compile(r'^([a-zA-Z0-9]+)')
ID_RE = re.compile(r'#([a-zA-Z0-9_-]+)')
CLASS_RE = re.compile(r'\.([a-zA-Z][a-zA-Z0-9_-]*)')


def parse_emmet_rule(rule):
    """
    Разбирает правило emmet на html тег, его id и css-класс(ы).

    >>> parse_emmet_rule('p')
    ('p', '', [])
    >>> parse_emmet_rule('h1.big.green#head')
    ('h1', 'head', ['big', 'green'])
    """
    match = TAG_RE.match(rule)
    tag = match.group(1) if match else 'p'  # если тега нет, то <p>
    match = ID_RE.search(rule)
    tag_id = match.group(1) if match else ''
    classes = CLASS_RE.findall(rule)
    return (tag, tag_id, classes)


def render_property(key, value):
    """
    Рендер в html одной пары ключ/значение json-объекта.

    Ключ может быть как простым тегом, так и правилом в стиле emmet.

    >>> render_property('p', 'text')
    '<p>text</p>'
    >>> render_property('p.big#head', 'text')
    '<p id="head" class="big">text</p>'
    """
    (tag, tag_id, classes) = parse_emmet_rule(key)
    id_attr = ' id="{}"'.format(tag_id) if tag_id else ''
    class_attr = ' class="{}"'.format(' '.join(classes)) if classes else ''
    return '<{tag}{id_attr}{class_attr}>{content}</{tag}>'.format(
        tag=tag,
        id_attr=id_attr,
        class_attr=class_attr,
        content=render_json(value),
    )


def render_object(json_obj):
    """
    Рендер в html всех полей json-объекта.

    >>> render_object({'h1': 'head', 'p': 'text'})
    '<h1>head</h1><p>text</p>'
    """
    return ''.join(
        render_property(key, value) for key, value in json_obj.items()
    )


def render_list(json_list):
    """
    Рендер в html всех json-объектов в списке.

    >>> render_list([{'h1': 'head'}, {'h2': 'head 2', 'p': 'text'}])
    '<ul><li><h1>head</h1></li><li><h2>head 2</h2><p>text</p></li></ul>'
    """
    items = ''.join(
        '<li>{}</li>'.format(render_json(json_obj)) for json_obj in json_list
    )
    return '<ul>{}</ul>'.format(items)


def render_json(json_data):
    """
    Рендер в html произвольных json-данных (строки/объекта/списка).

    >>> render_json('text')
    'text'
    >>> render_json({'p': 'text'})
    '<p>text</p>'
    >>> render_json([{'p': 'text'}, 'text2'])
    '<ul><li><p>text</p></li><li>text2</li></ul>'
    """
    if isinstance(json_data, str):
        return html.escape(json_data)
    if isinstance(json_data, list):
        return render_list(json_data)
    return render_object(json_data)


if __name__ == '__main__':
    file_name = sys.argv[1] if len(sys.argv) > 1 else 'source.json'
    with open(file_name) as f:
        print(render_json(json.load(f, object_pairs_hook=OrderedDict)))
